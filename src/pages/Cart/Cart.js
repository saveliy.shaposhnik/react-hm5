import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../components/Button/Button';
import CartForm from '../../components/Form/CartForm';
import Items from '../../components/Items/Items';
import NoItems from '../../components/NoItems/NoItems';
import { TOGGLE_FORM } from '../../store/form/types';

export default function Cart(props) {

    const { setLiked, onClick } = props;
    const items = useSelector(state => state.items.items.data);
    const [isCartOpen] = useState(true);
    const dispatch = useDispatch()
    const isFormActive = useSelector(state => state.form.form.isActive);
    const cart = JSON.parse(localStorage.getItem("Cart")) || []
    let cartItems = []

    const getLocalCart = () => {
        items.forEach(el => {
            cart.forEach(element => {
                if (element === el.id) {
                    cartItems.push(el);
                }
            });
        })
    }
    if (cart.length === 0) {
        return <NoItems />
    }
    const openForm = () => {
        dispatch({type:TOGGLE_FORM,payload:true})
    }
    return (
        <>
            {isFormActive && <CartForm/>}
            <div className="cards-div">
                {getLocalCart()}
                {cartItems.map(el => {
                    return <Items key={el.id} items={el} setLiked={setLiked} onClick={onClick} isCartOpen={isCartOpen} />
                })}
            </div>
            <Button onClick={openForm} backgroundColor="white" text="Buy" className="buy-btn"/>
        </>
    )
}
