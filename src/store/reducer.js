import { combineReducers } from "redux";
import itemsReducer from "./items/reducer"
import modalReducer from "./modal/reducer"
import userReducer from "./user/reducer"
import formReducer from "./form/reducer"

const reducer = combineReducers({
    modal: modalReducer,
    items: itemsReducer,
    user: userReducer,
    form:formReducer,
})

export default reducer;