import { CREATE_USER } from "./types";

const initialStore = {
    user:{
        data:{
            name:"",
            surname:"",
            age:"",
            address:"",
            phoneNumber:"",
        }
    }
}


const reducer = (state = initialStore,action) => {
    switch(action.type){
        case CREATE_USER:
            return ({...state,user:{...state.user,data:{name:action.payload.name,surname:action.payload.surname,age:action.payload.age,address:action.payload.address,phoneNumber:action.payload.phoneNumber}}})
        default:
            return state
    }
}

export default reducer;