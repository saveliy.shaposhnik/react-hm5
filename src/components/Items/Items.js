import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import "./Items.scss"
import React from 'react'

export default function Items(props) {
    const { setLiked, onClick, items, isCartOpen, isMainOpen } = props
    const { items: { isFavorite, Url, title, id, price, inCart,author } } = props;
    let text = ""
    let disabled = false;

    if (inCart) {
        text = "In cart"
        disabled = true;
    } else if (isMainOpen) {
        text = "Add to Cart";
        disabled = false;
    }
    
    return (
        <div className="card" id={id}>
            <img src={Url} alt="Img" width="230px" height="230px" />
            <h3>"{title}"</h3>
            <h5>{author}</h5>
            <p>Price:{price}$</p>
            <div className="card-footer">
                <Icon
                    onClick={() => setLiked(items)}
                    color="#ffd700"
                    filled={isFavorite ? "#ffd700" : "none"}
                />
                {isCartOpen && <Button text="X" backgroundColor="inherit" className="closeBtn" onClick={() => onClick(items)} />}
                {isMainOpen && <Button disabled={disabled} text={text} backgroundColor="black" className="openBtn" onClick={() => onClick(items)} />}
            </div>
        </div>
    )
}


