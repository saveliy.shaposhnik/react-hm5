import React from 'react'
import { Formik, Form } from "formik";
import * as Yup from "yup"
import MyInput from './MyInput';
import "./CartForm.scss"
import { useDispatch, useSelector } from 'react-redux';
import { CREATE_USER } from '../../store/user/types';
import { TOGGLE_FORM } from '../../store/form/types';
import { SET_ITEMS } from '../../store/items/types';


export default function CartForm() {
    const dispatch = useDispatch()
    const items = useSelector(state => state.items.items.data);

    const submitForm = (values) => {
        dispatch({type:TOGGLE_FORM,payload:false})
        localStorage.removeItem("Cart")
        dispatch({type:CREATE_USER,payload:values})
        const newArr = items.map(el => {
            el.inCart = false
            return el
        })
        dispatch({type:SET_ITEMS,payload:newArr})
    }

    const validationFormSchema = Yup.object().shape({
        name: Yup.string()
            .required('Required')
            .min(2, 'Too Short!'),
        surname: Yup.string()
            .required('Required')
            .min(3, 'Too Short!'),
        age: Yup.number()
            .required('Required')
            .positive("Must be positive")
            .moreThan(17,"You must be more then 18"),
        address: Yup.string()
            .required('Required'),
        phoneNumber: Yup.number()
            .required('Required')
            .min(6, 'Too Short!'),
    })

    return (
        <div>
            <Formik
                initialValues={{
                    name: '',
                    surname: '',
                    age: '',
                    address: '',
                    phoneNumber: '',
                }}
                onSubmit={submitForm}
                validationSchema={validationFormSchema}
            >
                {(formikProps) => {
                    console.log(formikProps)
                    return (
                        <Form className="form">
                            <h2>Login</h2>
                            <MyInput name="name" type="text" label="Name" />
                            <MyInput name="surname" type="text" label="Surname" />
                            <MyInput name="age" type="text" label="Age" />
                            <MyInput name="address" type="text" label="Address" />
                            <MyInput name="phoneNumber" type="text" label="Phone number" />
                            <div>
                                <button type="submit" className="submit">Checkout</button>
                            </div>
                        </Form>
                    );
                }}
            </Formik>
        </div>
    )
}
